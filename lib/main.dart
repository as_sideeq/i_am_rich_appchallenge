import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.blueGrey,
          appBar: AppBar(
            title: Text('I am Rich'),
            backgroundColor: Colors.black38,
          ),
          body: Center(
            child: Image(
              image: AssetImage('images/transparent.png'),
            ),
          ),
        ),
      ),
    );
